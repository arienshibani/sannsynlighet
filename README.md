# Formler til Sannsynlighetsregning   #

Illustrerer et par konsepter fra INFO103: Informasjon og Kunnskap[INFO103: Informasjon og Kunnskap](https://www.uib.no/emne/INFO103).

* Link - http://wildboy.uib.no/~ash010/sannsynlighet/
* Bayes Theorem
* Vilkårlig Sannsynlighet (Conditional Probability)
* nCr (Utregning av kombinasjoner)
* nPr (Utregning av permutasjoner)


