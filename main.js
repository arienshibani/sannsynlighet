function bayes_theorem() {
    var PA = parseFloat(document.getElementById('probA').value.replace(/,/g, ".")); //Hypothesis
    var PBGA = parseFloat(document.getElementById('proBgivenA').value.replace(/,/g, ".")); //Hit-rate
    var PB = parseFloat(document.getElementById('proBgivenNotA').value.replace(/,/g, ".")); //False-alarm
    var PAGB;

    //Value checks
    if (PA <= 0 | PA >= 1 | PA === NaN) {
        {
            alert("Hypotesen P(A), Må være mellom 0 og 1")
        };
    } else
    if (PBGA <= 0 | PBGA >= 1 | PA === NaN) {
        {
            alert("Hit-raten (B|A), må være mellom 0 og 1")
        };
    } else
    if (PB <= 0 | PB >= 1 | PA === NaN) {
        {
            alert("Miss-raten (B|notA) må være mellom 0 og 1")
        };
    }

    //Bayes theorem
    PAGB = (PBGA * PA) / PB
    document.getElementById('probAgivenB').value = "= " + PAGB.toString().substr(0, 6);
    console.log(PAGB);

}

function conditional_probability() {
    var PA = parseFloat(document.getElementById('pA').value.replace(/,/g, ".")); //Probability of A happening
    var PAnB = parseFloat(document.getElementById('pAandB').value.replace(/,/g, ".")); //Probability of A and B happening
    var PBGA;

    //Value checks
    if (PA <= 0 | PA >= 1 | PA === NaN) {
        {
            alert("P(A), Må være mellom 0.0001 og 1")
        };
    } else
    if (PAnB <= 0 | PAnB >= 1 | PA === NaN) {
        {
            alert("P(A∩B), må være mellom 0 og 1")
        };
    }

    //Conditional Probability
    //P(B|A) = P(A ∩ B) / P(A)
    PBGA = (PAnB / PA);
    document.getElementById('probBgivenA').value = "= " + PBGA.toString().substr(0, 6);
    console.log(PGBA);

}

function nCr(N, R) {
    var N = parseFloat(document.getElementById('ncr_n').value.replace(/,/g, "."));
    var R = parseFloat(document.getElementById('ncr_r').value.replace(/,/g, "."));
    var NCR;

    if (N < R) {
        {
            alert("En delmengde kan ikke være større en overmengden.")
        };
    }

    if (R < 0) {
        {
            alert("R må være større en 0.")
        };
    }

    if (N === R) {
        NCR = factorial(N);
        document.getElementById('combinations').value = "= " + NCR.toString();
        return NCR;
    }


    NCR = factorial(N) / (factorial(R) * factorial(N - R))
    document.getElementById('combinations').value = "= " + NCR.toString();
    return NCR;

}

function nPr(N, R) {

    var N = parseFloat(document.getElementById('npr_n').value.replace(/,/g, "."));
    var R = parseFloat(document.getElementById('npr_r').value.replace(/,/g, "."));
    var NPR;

    if (N < R) {
        {
            alert("En delmengde kan ikke være større en overmengden.")
        };
    }

    if (R < 0) {
        {
            alert("R må være større en 0.")
        };
    }

    if (N === R) {
        NPR = factorial(N);
        document.getElementById('permutations').value = "= " + NPR.toString();
        return NPR;
    }


    NPR = factorial(N) / factorial(N - R)
    document.getElementById('permutations').value = "= " + NPR.toString();
    return NPR
}

function openFormula(evt, formulaName) {
    var i, formula, tablinks;
    formula = document.getElementsByClassName("formula");
    for (i = 0; i < formula.length; i++) {
        formula[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(formulaName).style.display = "block";
    evt.currentTarget.className += " active";
}

function factorial(x) {

    if (x === 0) {
        return 1;
    }
    return x * factorial(x - 1);

}
